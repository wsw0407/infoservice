# 大学生校园服务信息管理系统

#### Description
系统是一个服务于大学生的平台，进行信息的收集管理和展示，系统提供首页显示校园风采、文化历史浏览校园信息，可以查看浏览校园、网络赛事活动等信息，能在留言平台进行交流发布留言、评论和回复，可以上传分享自己的文档、软件和物品资源，能够查询课程信息并可以导出图片，针对当下疫情提供实时疫情状况信息的浏览，提供数据的可视化展示，管理员登录管理整个系统的各个模块信息的发布和管理

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
